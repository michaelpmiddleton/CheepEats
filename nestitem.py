class NestItem:
    def __init__(self, name, location, address, place_id):
        self.name = name
        self.location = location
        self.address = address
        self.place_id = place_id

    def toString(self):
        return self.name + '\t' + self.address

