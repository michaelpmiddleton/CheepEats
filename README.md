# CheepEats
STONEHILL COLLEGE - Files that comprise my final project for CSC 324 with Prof. Bob Dugan.<br>
<br>
<br>
If you would like to Download this assignment, feel free to do so. You may also edit the documents to your satisfaction. If used anywhere, I request credit via commentation on any file that uses my code.
<br>
<br>
<h2>Objective:</h2>
This repository was created so that both my professor and I could track the progress of each my final project.
<br>
<br>
<br>
<h2>Got Questions?</h2>
Feel free to send me an email. I will do my best to get back to you in an efficient manner. [Click here to send me an email.](mailto:mmiddleton@students.stonehill.edu)
