import dataset
from user import User
from nestitem import NestItem

class UserDao:
    def __init__ (self):
        self.db = dataset.connect ('sqlite:///users.db')
        self.table = self.db['users']
        self.nest = None

    def rowToUser (self, row):
        user = User (row['userid'], row['password'], row['emailaddr'])
        return user



    def userToRow (self, user):
        row = dict(userid=user.userid, password=user.password, emailaddr=user.emailaddr)
        return row



    def selectByUserId (self, userid):
        rows = self.table.find (userid = userid)
        result = None

        if (rows is None):
            print ('ERROR:\t[UserDao.selectByUserId()]\tCannot find user with ID of \'' + userid + '\'.')
            result = None

        else:
            count = 0
            for row in rows:
                if (count > 0):
                    print ('ERROR:\t[UserDao.selectByUserId()]\tFound more than one user with userid \'' + userid + '\'.')
                    return None

                else:
                    result = self.rowToUser (row)
                    count += 1

        return result



    def selectAll (self):
        rows = self.table.all ()
        results = []

        for row in rows:
            results.append (self.rowToUser (row))

        return results



    def insert (self, user):
        self.table.insert (self.userToRow (user))
        self.db.commit ()



    def update (self, user):
        self.table.update (self.userToRow (user), ['userid'])
        self.db.commit ()



    def delete (self, user):
        self.table.delete (user = user)
        self.db.commit ()



    def populate (self):
        self.table.insert (self.userToRow (User ('admin', 'Administrator', 'cheepeatsofficial@gmail.com')))
        self.table.insert (self.userToRow (User ('mmiddleton', 'password', 'mpm1996@gmail.com')))
        self.db.commit ()

    def isValid (self, userid, password):
        account = self.selectByUserId (userid)

        if (account is not None):
            if (account.password == password):
                print ('INFO:\t[UserDAO.isValid()]\tAccount pair is valid. Logging in...')
                return True

            else:
                print ('INFO:\t[UserDAO.isValid()]\tAccount found, passwords do not match.')
        
        else:
            print ('INFO:\t[UserDAO.isValid()]\tCannot find username with userid \'' + userid + '\'.')

        return False

    def isValidPair (self, userid, emailaddr):
        recover = self.selectByUserId (userid)

        if (recover is not None):
            if (recover.emailaddr == emailaddr):
                return recover
            else:
                print ('ERROR:\t[UserDAO.isValidPair()]\tGiven username and email address do not match!')

        return None
        
    def isAvailable (self, user):
        available = self.selectByUserId (user.userid)
        if (user == 'users'):
            available = 'NOT AVAILABLE'

        if (available is None):
            print ('INFO:\t[UserDAO.isAvailable()]\tAccount \'' + user.userid + '\' is available.' )
            return True

        else:
            print ('INFO:\t[UserDAO.isAvialble()]\tAccount is already taken!')
            return False

    #   /   user
    #   =============================================================================================================
    #   nest

    #
    #   NESTITEMTOROW:      Convert 'NestItem' object to dictionary item.
    #
    def nestItemToRow (self, nestItem):
        row = dict(name=nestItem.name, location=nestItem.address, address=nestItem.address, place_id=nestItem.place_id)
        return row

    #
    #   ROWTONESTITEM:      Convert dictionary item to 'NestItem' object.
    #
    def rowToNestItem (self, row):
        nestItem = NestItem (row['name'], row['location'], row['address'], row['place_id'])
        return nestItem

    #
    #   INSERTNESTITEM:     Inserts a collection into the table matching the user's userid.
    #                       NOTE: This method takes a 'row' NOT a NestItem.
    #
    def insertNestItem (self, nestItem):
        if (self.isDuplicate (nestItem.get ('place_id'))):
            print ('INFO:\t\tAttempt to add prexisting favorite detected.')
            return False

        else:
            self.nest.insert (nestItem)
            self.db.commit ()
            return True

    #
    #   REMOVENESTITEM:     Removes a collection from the table matching the user's userid.
    # 
    def removeNestItem (self, nestItem):
        self.nest.delete (nestItem = nestItem)
        self.db.commit ()

    #
    #   ISDUPLICATE:    Checks the DB table for NestItem with a matching place_id. Returns true if prexisting item is found. 
    #
    def isDuplicate (self, place_id):
        currentNest = self.nest.all ()

        for nestItem in currentNest:
            if (place_id == nestItem.get('place_id')):
                return True

        return False

    #
    #   ACCESSUSERNEST:     Returns the list of NestItems in the Nest DB.
    #
    def accessUserNest (self, userid):
        self.nest = self.db[userid]
        nestItems = self.nest.all ()
        results = []

        for nestItem in nestItems:
            results.append (self.rowToNestItem (nestItem))

        return results

    #
    #   SELECTBYPLACEID:    Returns a NestItem object that matches the given place_id.
    #
    def selectByPlaceId (self, placeid):
        nestItems = self.nest.all ()

        for nestItem in nestItems:
            if (nestItem.get ('place_id') == placeid):
                return nestItem

        print ('ERROR: NO MATCHES FOUND!!!')
        return None
