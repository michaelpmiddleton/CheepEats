#   Flask Imports:
from flask import Flask
from flask import abort, redirect, url_for
from flask import request
from flask import render_template
from flask import session
from flask import jsonify

#   Database Imports:
import logging
from jsonpickle import encode
from jsonpickle import decode
import sys 

#   Michael's Imports:
from emailservice import EmailService
from userdao import UserDao
from nestitem import NestItem
import urllib 
import random


app = Flask (__name__)
null = None

#################################################
#           ROOT                               #
#################################################
@app.route ('/')
def index ():
    session['background'] = encode (str (random.randint (1, 9)))

    # This will keep the user logged in until they click 'logout'
    try:
        if decode (session['userid']) is not None:
            return redirect (url_for ('cheepeats'))
    except:
        print ('New user has access the page for the first time! YAY!')

    return redirect (url_for ('login'))




#################################################
#           LOGIN                               #
#################################################
@app.route ('/login', methods = ['POST', 'GET'])
def login ():
    error = None
    dao = UserDao ()

    backgroundIndex = str (random.randint (1, 9))
    
    while (decode (session['background']) == backgroundIndex):
        backgroundIndex = str (random.randint (1, 9))

    session['background'] = encode (backgroundIndex)

    #print ('INFO:\t[Borb.login()]\tGenerated background with index:\t' + backgroundIndex)

    if (request.method == 'POST'):
        if (dao.isValid (request.form['userid'], request.form['password'])):
            session['userid'] = encode (request.form['userid'])
            return redirect (url_for ('cheepeats'))

        else:
            error = 'Woops! We don\'t seem to have an account with those credentials.'
            #error = dao.selectByUserId (request.form['userid']).password

    #   This will display if the credentials were bad OR the call was a GET request. (ie. First time accessing the page.)
    return render_template ('login.html', **locals ())



#################################################
#           CREATE ACCOUNT                      #
#################################################
@app.route ('/createaccount', methods = ['POST', 'GET'])
def createAccount ():
    #   Locals:
    error = None
    dao = UserDao ()
    background = decode (session ['background'])
    es = EmailService ()

    if (request.method == 'POST'):
        row = dict (userid=request.form['userid'], password=request.form['password'], emailaddr=request.form['emailaddr'])
        temp = dao.rowToUser (row)

        if (dao.isAvailable (temp)):
            dao.insert (temp)
            if (es.sendSignupEmail (temp)):
                print ('INFO:\t[Borb.createAccount()]\tCreated Account: ' + str (row))
                redirect (url_for ('login'))
            else:
                print ('Error sending account creation email. Must be invalid email addess. EMAIL ADDRESS: ' + row.get('emailaddr'))
                error = 'It appears you have entered an invalid email adress. Please try again.'

        else:
            error = 'Darn! That account appears to have already been taken'
            print ('INFO:\t[Borb.createAccount()]\tCould not create account.')

    return render_template ('createaccount.html', **locals ()) 




#################################################
#           CHEEPEATS MAIN PAGE                 #
#################################################
@app.route ('/cheepeats', methods = ['POST', 'GET'])
def cheepeats ():
    userid = decode(session ['userid'])

    # We can't allow access to this page without having you log in first.
    if decode (session['userid']) is None:
        print ('ERROR:\t[Borb.cheepeats()]\tAttempted to access maps page without signing in. Redirecting to login screen...')
        return redirect (url_for ('login'))

    dao = UserDao ()
    nest = dao.accessUserNest (userid)

    insertIntoNest = request.args.get ('insert')
    removeFromNest = request.args.get ('remove')
    updateNest = request.args.get ('update')


    #
    #   Request from Front End to UPDATE Client-side collection of places.
    #
    if (updateNest != None):
        print ('INFO:\t\tNest UPDATE request received.')
        nest = dao.accessUserNest (userid)
        nestCollection = {}

        for nestItem in nest:
            nestCollection[str (nest.index (nestItem))] = dao.nestItemToRow (nestItem)

        return jsonify (dict (containsElements=any(nestCollection),places=nestCollection, imgURL=url_for ('static', filename='img/close_button.png')))

    #
    #   Request from Front End to add item to user-nest.db:
    #
    if (insertIntoNest != None):
        print ('INFO:\t\tNest INSERT request received.')
        queryParams = (insertIntoNest.split ('&'))

        # Separate and format the Parameters:
        for param in queryParams:
            index = queryParams.index (param)
            queryParams[index] = param.split ('=')[1]
            
            # Clean the paramters:
            for c in range (0, len (queryParams)):
                queryParams[c] = urllib.parse.unquote (queryParams[c]) 
                queryParams[c] = str.replace (queryParams[c], '+', ' ')
        
        insertIntoNest = dict(name=queryParams[0], location=queryParams[1], address=queryParams[2], place_id=queryParams[3])
        success = dao.insertNestItem (insertIntoNest)
        insertRequest = jsonify (dict (status=success, nestItem=insertIntoNest, imgURL=url_for ('static', filename='img/close_button.png')))
        return insertRequest

    #TODO:
    if (removeFromNest != None):
        print ('INFO:\t\tNest REMOVE request received.')
        nest = dao.accessUserNest (userid)
        print (nest)
        toBeRemoved = dao.selectByPlaceId (removeFromNest)
        dao.removeNestItem (toBeRemoved)
        return jsonify (toBeRemoved)


    return render_template ('cheepeats.html', **locals ()) 




# Method for adding to nest:
@app.route ('/addtonest', methods= ['POST', 'GET'])
def addtonest ():
    print ('We got this far.')
    return




#################################################
#           ACCOUNT RECOVERY                    #
#################################################
@app.route ('/accountrecovery', methods = ['POST', 'GET'])
def accountrecovery ():
    error = None
    dao = UserDao ()
    es = EmailService ()
    
    background = decode (session['background'])

    if (request.method == 'POST'):
        if (dao.isValidPair (request.form['userid'], request.form['emailaddr'])):
            es.recoverPassword (request.form['userid'])
            return redirect (url_for ('login'))

        else:
            error = 'Hmm... We don\'t seem to have an account with those credentials.'

    #   This will display if the credentials were bad OR the call was a GET request. (ie. First time accessing the page.)
    return render_template ('recoveraccount.html', **locals ())





#################################################
#           LOGOUT:                             #
#################################################
@app.route ('/logout', methods = ['POST', 'GET'])
def logout ():
    userid = decode (session['userid'])
    session['userid'] = encode (None)
    print ('INFO:\t[Borb.logout()]\tUser \'' + userid + '\' has successully been logged out.')

    return redirect (url_for ('login'))



#################################################
#           GIT FORWARWDING:                    #
#################################################
@app.route ('/git', methods = ['POST', 'GET'])
def git ():
    try:
        userid = decode (session['userid'])
        print ('INFO:\t\tNavigating user \'' + userid + '\' to the GitHub Repo.')
    except:
        print ('ERROR:\t\tEither the user is currently not logged in OR there was an authentication error.')

    return redirect ('https://www.github.com/mmiddleton/CheepEats')



#################################################
#           MAIN                                #
#################################################
if __name__ == "__main__":
    app.secret_key = 'ProjectB0RBDesktopPrototype'
    app.run (host = 'localhost', debug = True)
