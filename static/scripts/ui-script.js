/*
    Authors:    Michael Middleton (mmiddleton)
    Project:    CheepEats [DESKTOP]
    Created:    26 April 2016
    Updated:    11 May 2016 - 3:05 EST
*/
var $SCRIPT_ROOT = "http://localhost:5000";

$(function () {
    //  Variables:
    var current;
    var currentNest = [];
    var parameters = $("#parameters");
    var foodTypes = "<select id='food-type-input'><option value=‘American’>American</option><option value=‘Barbeque’>BBQ</option><option value=‘Chinese’>Chinese</option><option value=‘French’>French</option><option value=‘Hambgurger’>Hamburger</option><option value=‘Indian’>Indian</option><option value=‘Italian’>Italian</option><option value=‘Japanese’>Japanese</option><option value=‘Pizza’>Pizza</option><option value=‘Mexican’>Mexican</option><option value=’Seafood’>Seafood</option><option value=‘Steak’>Steakhouse</option><option value=‘Sushi’>Sushi</option><option value=’Thai’>Thai</option></select>"; 
    var money = "<form><input id='money-input' type='number' step='.01' min='0' max='200' value='0.00'></form>";
    var loc = "<form><input id='loc-input' type='number' step='1' min='0' max='99999' value='02334'></form><p>- OR -</p><a id='useCurrentPosition' href='#'>Use your current location</a>";

    //  'Inits':
    $("h3").on ('click', function () {
        current = $(this).attr ('id');
        //alert (current);


        switch (current) {
            case 'Food-Type':
                parameters.html(foodTypes);
                break;
            case 'Money':
                parameters.html(money);
                break;
            case 'Location':
                parameters.html(loc);
                break;
            default:
                alert ('Something went wrong! Try reloading the page.')
        }
    });


    // 'Set up the nest & place transition':
    $("#PlacesAndNest").tabs ();



    // Clear textfield on click if has default data:
    $(".box > form > input").on ('click', function () {
        var DEFAULT = "";
        
        switch ($(this).attr ("id")) {
            case 'userid':
                DEFAULT = 'Username';
                break;

            case 'password':
                DEFAULT = 'Password';
                break;

            case 'emailaddr':
                DEFAULT = 'Email Address';
                break;

            default:
                console.log ('Error. ID: ' + $(this).attr ('id'));
                break;
        }

        if ($(this).val () == DEFAULT)
            $(this).val ("");
    });

    //  Reset textfield if empty:
    $(".box > form > input").on ('blur', function () {
        if ($(this).val () == "")
            switch ($(this).attr ('id')) {
                case 'userid':
                    $(this).val ('Username');
                    break;

                case 'password':
                    $(this).val ('Password');
                    break;

                case 'emailaddr':
                    $(this).val ('Email Address');
                    break;

                default:
                    console.log ('Error. ID: ' + $(this).attr ('id'));
                    break;
            }
    });


    // Click handler for the main search button.
    $("#cheep-cheep").on ('click', function () {
        var results;

        switch (current) {
            case "Food-Type":
                searchByFoodType ($("#food-type-input").val ());
                break;

            case 'Money':
                searchByPrice ($("#money-input").val ());
                break;

            case 'Location':
                searchByLocation (false); 
                break;

            default:
                alert ('Please select a search option first!');
                break;
       }
        
    });

    //  On 'Use current location' click:
    $("#parameters").on ('click', '#useCurrentPosition', function () {
        searchByLocation (true);
    });


    //  Select an item in the places list:
    $("#places").on ('click', '.pnItem', function () {
        if (currentMarker != undefined) {
            try {
                currentMarker.infowindow.close (); 
            }   catch (err) {}
        }   

        var key = $(this).context.children[2].textContent.toString (); //This will get the Place ID that is hidden on the page.
        for (c in listOfEstablishments) {
            if (key.match (listOfEstablishments[c].identifier)) {
                listOfEstablishments[c].marker = highlightMarker (listOfEstablishments[c].marker);
                map.setZoom(11);
            }
        }
    });

    //  Display Nest:
    $.getJSON (
            $SCRIPT_ROOT+'/cheepeats',
            {update: 'UPDATE REQUEST'},
            function (data) {
                currentNest = [];
                if (data.containsElements) {
                    for (i in data.places) {
                        currentNest.push (data.places[i]);
                    }

                   var htmlToBeSet = "";

                    for (index in currentNest)
                        htmlToBeSet += "<div class='pnItem'><img src='" + data.imgURL + "' class='hidden' alt='delete'><h4>" + currentNest[index].name + "</h4><p>Address: " + currentNest[index].address+ "</p><p class='hidden'>" + currentNest[index].place_id+ "</p></div>"; 

                    $('#nest').html (htmlToBeSet);
                }
                else
                    console.log ('User nest is currently empty.');
            }
    );

    
    //  Add to Nest:
    $('#add-to-nest').on ('click', function () {
        $.getJSON (
                $SCRIPT_ROOT+'/cheepeats',
                {insert: $.param({name: currentMarker.name, location: 'TO_BE_RESOLVED', address: currentMarker.address, place_id: currentMarker.place_id})},
                function (data) {
                    if (data.status == true) {
                        console.log (currentNest);

                        if (currentNest.length > 0) {
                            $('#nest').html ($('#nest').html () +  "<div class='pnItem'><img src='" + data.imgURL + "' class='hidden' alt='delete'><h4>" + data.nestItem.name + "</h4><p>Address: " + data.nestItem.address + "</p><p class='hidden'>" + data.nestItem.place_id + "</p></div>");
                        }
                        else {
                            $('#nest').html ("<div class='pnItem'><img src='" + data.imgURL + "' class='hidden' alt='delete'><h4>" + data.nestItem.name + "</h4><p>Address: " + data.nestItem.address + "</p><p class='hidden'>" + data.nestItem.place_id + "</p></div>");
                        }

                        currentNest.push (data.nestItem);
                    }
                    else
                        console.log (data.nestItem.name + ' is already in your Nest!');          
                }
        );
    });

    // TODO
    //  Remove from Nest:
    $('#nest').on ('click', 'img', function () {
        var toBeRemoved =  ($(this).context.parentNode.children[3].textContent);

        $.getJSON (
                $SCRIPT_ROOT+'/cheepeats',
                {remove: toBeRemoved},
                function () {
                    alert ('IT\'S WORKING!!!');          
                }
        );
    });

    $('#nest').on ('mouseenter', ' .pnItem',  function () {
            $(this).children ('img').attr ('class', 'show');
    });

    $('#nest').on ('mouseleave', '.pnItem', function () {
            $(this).children ('img').attr ('class', 'hidden');   
    });
});

function updatePlaces () {
    $("#places").html("");
    var divString = "";
    var temp;
    for (c in listOfEstablishments) {
        temp = "<div class='pnItem'><h4>" + listOfEstablishments[c].name + "</h4><p>Address: " + listOfEstablishments[c].address + "</p><p class='hidden'>" + listOfEstablishments[c].identifier + "</p></div>";
        divString += temp;
    }
    $("#places").html(divString);
}


