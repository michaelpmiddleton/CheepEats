/*
    Authors:    Michael Middleton (mmiddleton)
    Project:    CheepEats [DESKTOP]
    Created:    26 April 2016
    Updated:    10 May 2016 - 1:02 EST
*/

//
//  VARIABLES:
//
var geo;
var browserHasGPS = true;
var currentLoc = null;
var userPosition = null;
var DISTANCE = 16093;
var searchService;
var listOfEstablishments = []; // For later use... (Complete list of accurate places around user based on Google AND CheepEats DB.)
var map;
var currentMarker;
var tempMarker; // Used for searching by alternate location.



//
//  INITMAP:    Called by the Google Maps JS file, this is the map initializer.
//
function initMap () {
    var mapDiv = document.getElementById ('map');
    map = new google.maps.Map (mapDiv, { 
        zoom: 15,
        disableDefaultUI: true
    });
    geo = new google.maps.Geocoder ();

    // Attempt to use current location:
    navigator.geolocation.getCurrentPosition (
            function (position) { //Success
                currentLoc = new google.maps.LatLng (position.coords.latitude, position.coords.longitude);  
                userPosition = new google.maps.LatLng (position.coords.latitude, position.coords.longitude);  
                map.setCenter (currentLoc);
                console.log ('Current position: ' + currentLoc);
            },
            function () { //Failure
                if (navigator.geolocation)
                    alert ("Browser has GPS capabilites, there was an error.");
                else
                    alert ("Browser has no GPS. What are we gonna do?");

                currentLoc = new google.maps.LatLng (42.3161225,-71.0467654); //Set default to BC High at the moment.
                userPos = new google.maps.LatLng (42.3161225,-71.0467654); //Set default to BC High at the moment.
                browserHasGPS = false;
                map.setCenter (currentLoc);
            }
    ); 

    

    //  This gives us access to the Google Places API (ie. The place search function & the Info Window onject):
    infoBubble = new google.maps.InfoWindow ();
    searchService = new google.maps.places.PlacesService (map);





    // This style is called 'YouthUp' and it was created by Emanuel Roy on SnazzyMaps.
    // By inserting these comments, the developers of CheepEats sign that this style is his work and that all credit for it is due to him.
    var styles = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"all","stylers":[{"visibility":"on"},{"color":"#00858a"}]},{"featureType":"administrative.country","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.neighborhood","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f6ebcb"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.fill","stylers":[{"color":"#f7f1df"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"on"},{"color":"#f3dd9d"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"},{"visibility":"on"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#f8a179"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#e6dcbd"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#3dbbc2"}]}]
    //  END OF 'YouthUp' STYLE  //
   
    map.setOptions ({styles: styles});
    

    // This will be called ONCE, after the map is officially idle.
    google.maps.event.addListenerOnce (map, 'tilesloaded', function () {
        placeUser (); 
    });
}



//
//  SEARCHBYFOODTYPE:   Generates search function based on value of 'foodType'
//
function searchByFoodType (foodType) {
    if (tempMarker != undefined)
        tempMarker.setMap (null);

    //TODO;
    searchService.textSearch ({
        location: currentLoc,
        radius: DISTANCE,
        query: foodType + " food",
        type: 'restaurant'
    }, preCallback);
}



//
//  SEARCHBYPRICE:  Generates search function based on Google's price rating and the CheepEats DB entries.
//
function searchByPrice (price) {
    currentLoc = userPosition;
    if (tempMarker != undefined)
        tempMarker.setMap (null);

    var rating;
    
    //  CheepEats DB:
    //  TODO

    //  Google Price Rating:
    if (price < 10)
        rating = 0;
    else if (price <= 20)
        rating = 1;
    else if (price <= 35)
        rating = 2;
    else if (price <= 50)
        rating = 3;
    else
        rating = 4;
    
    searchService.nearbySearch ({
        location: currentLoc,
        radius: DISTANCE,
        type: 'restaurant',
        maxPriceLevel: rating
    }, callback);

}



//
//  SEARCHBYLOCATION:   Generates generic search for 'restaraunts' 15 miles near 'loc'.
//
function searchByLocation (usePosition) {
    var loc = $('#loc-input').val ();
    geo.geocode ({'address': loc}, function (results, status) {
       if (status === google.maps.GeocoderStatus.OK) {
           
            //  Use current Position:
            if (usePosition) {
                currentLoc = userPosition;
                if (tempMarker != undefined)
                    tempMarker.setMap (null);
            }
            //  Use input zipcode:
            else {
                currentLoc = results[0].geometry.location;
                placeTempOrigin (results[0].formatted_address);
            }
            //Set map center regardless:
            map.setCenter (currentLoc);


            //  Generic food search:
            searchService.nearbySearch ({
                location: currentLoc,
                radius: DISTANCE,
                type: 'restaurant',
            }, callback);
       }
       else {
            alert ('ERROR WITH SEARCH FUNCTION: ' + status);
       }    
    });
}

//
//  PRECALLBACK: Moves data in the 'results' for ease of access in 'callback()'.
//
function preCallback (results, status) {
    for (c in results)
        results[c].vicinity = results[c].formatted_address;
    callback (results, status);
}

//
//  CALLBACK:   Calls 'placeMarker' and 'updatePlaces'[UI-SCRIPT] for each location in 'results'.
//
function callback (results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
        //console.log (results);
        clearMap ();
        listOfEstablishments = [];

        for (c in results) {
            results[c].marker = placeMarker (results[c]);

            if (results[c].rating == undefined)
                results[c].rating = 'n/a';

            results[c].marker.name = results[c].name;
            results[c].marker.address = results[c].vicinity;
            results[c].marker.rating = results[c].rating;
            results[c].marker.place_id = results[c].place_id;

            //  Add listened to marker:
            results[c].marker.addListener('click', function() {
                if (currentMarker != undefined) {
                    try {
                        currentMarker.infowindow.close ();
                    }   catch (err) {}
                }
                
                highlightMarker ($(this)[0]);
                var infowindow = new google.maps.InfoWindow ({
                    content: "<div class='infowindow'><h4>" + $(this)[0].name+ "</h4><p>Address: " + $(this)[0].address + "</p><p>Rating: " + $(this)[0].rating + "</p><p>In DB: false</p></div>"
                });
                $(this)[0].infowindow = infowindow;
                infowindow.open (map, $(this)[0]);
            });

            //  Add object with attributes for ease of access.
            listOfEstablishments.push ({
                name: results[c].name,
                identifier: results[c].place_id + "",
                address: results[c].vicinity,
                rating: results[c].rating,
                position: results[c].geometry.location,
                marker: results[c].marker,
                bubble: results[c].bubble
            });
        }
        updatePlaces ();
        map.setZoom (11);
    }
    else
        alert (status);
        //console.log ('ERROR: Premature call to callback from searchService.');
}



//
//  PLACEMARKER:    Places marker on 'map' at given 'place'.
//
function placeMarker (place) {
    var marker = new google.maps.Marker ({
        map: map,
        position: place.geometry.location
    });
    return marker;
}


//
//  PLACEUSER:  Places a 'user.png' icon on the user's location.
//
function placeUser () {
    console.log ('User marker has been placed at coords: ' + currentLoc);
    var linkedImage = '/static/img/user.png';
    var marker = new google.maps.Marker ({
        map: map,
        position: currentLoc,
        icon: linkedImage
    });
}

//
//  PLACETEMPORIGIN:    Places a blue icon on the origin of the zip code entered.
//
function placeTempOrigin (town) {
    if (tempMarker != undefined)
        tempMarker.setMap (null);

    console.log ('Temporary origin set to: ' + currentLoc);
    var marker = new google.maps.Marker ({
        map: map,
        position: currentLoc,
        icon: 'http://maps.gstatic.com/mapfiles/markers2/boost-marker-mapview.png'
    });

    new google.maps.InfoWindow ({
        content: "<div class='infowindow'><h4>" + town + "</h4></div>"    
    }).open(map, marker);
   
    tempMarker = marker;
}

//
//  HIGHLIGHTMARKER: Changes icon of 'marker' to a green one and reverts any previous selection back to default.
//
function highlightMarker (marker) {
    // Reset previously selected  marker to the default red.
    if (currentMarker != undefined){
        currentMarker.setMap (null);
        currentMarker.setIcon ('http://www.mapnudge.com/assets/front/gmap/images/spotlight-poi.png');
        if (isInLOE (currentMarker))
            currentMarker.setMap (map);
    }

    // Update the current marker color and set 'currentMarker' to 'marker'.
    marker.setMap(null);
    marker.setIcon ('http://maps.gstatic.com/mapfiles/markers2/icon_green.png');
    marker.setMap (map);
    currentMarker = marker;
    map.setCenter (marker.position);
    return marker;
}

//
//  CLEARMAP:   Removes previous pips on map.
//
function clearMap () {
   for (c in listOfEstablishments) {
        listOfEstablishments[c].marker.setMap (null);
   }
}

//
//  ISINLISTOFESTABLISHMENTS: Checks to see whether given marker is in list of esatblishments
//
function isInLOE (marker) {
    placeid = marker.place_id+"";
    for (c in listOfEstablishments) {
        if (placeid.match (listOfEstablishments[c].identifier)) {
            return true;
        }
    }
    return false;
}
